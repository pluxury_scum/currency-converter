package com.Gvozdev.CurrencyConverter.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class History {
    @Id
    @GeneratedValue
    private Integer id;

    private String sourceValute;
    private String targetValute;
    private Double sourceSum;
    private Double convertedSum;
    private String queryDate;

    public History() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSourceValute() {
        return sourceValute;
    }

    public void setSourceValute(String sourceValute) {
        this.sourceValute = sourceValute;
    }

    public String getTargetValute() {
        return targetValute;
    }

    public void setTargetValute(String targetValute) {
        this.targetValute = targetValute;
    }

    public Double getSourceSum() {
        return sourceSum;
    }

    public void setSourceSum(Double sourceSum) {
        this.sourceSum = sourceSum;
    }

    public Double getConvertedSum() {
        return convertedSum;
    }

    public void setConvertedSum(Double convertedSum) {
        this.convertedSum = convertedSum;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }
}
