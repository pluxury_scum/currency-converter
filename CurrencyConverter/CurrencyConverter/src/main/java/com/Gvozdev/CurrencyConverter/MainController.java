package com.Gvozdev.CurrencyConverter;

import com.Gvozdev.CurrencyConverter.domain.Currency;
import com.Gvozdev.CurrencyConverter.domain.History;
import com.Gvozdev.CurrencyConverter.repos.CurrencyRepo;
import com.Gvozdev.CurrencyConverter.repos.HistoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private CurrencyRepo currencyRepo;

    @Autowired
    private HistoryRepo historyRepo;

    private History history;
    private static double rateCoefficient;

    @GetMapping("/")
    public String loadGreetingPage() {
        return "greeting";
    }

    @GetMapping("/main")
    public String loadMainPage(
            @RequestParam(name = "charcode", required = false, defaultValue = "AUD") String charcode,
            @RequestParam(name = "name", required = false, defaultValue = "Выбрать валюту") String name,
            Model model
    ) {
        setHistoryRepo(charcode);

        List<Currency> currencyList = currencyRepo.findByCharCode(charcode);
        String rate = currencyList.get(0).getValue().replace(',', '.');
        rateCoefficient = Double.parseDouble(rate);

        model.addAttribute("rateCoefficient", rateCoefficient);
        model.addAttribute("name", name);
        return "main";
    }

    public void setHistoryRepo(String charcode) {
        history = new History();
        history.setSourceValute(charcode);
        history.setTargetValute("RUB");
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String queryDate = dateFormat.format(date);
        history.setQueryDate(queryDate);
    }

    @PostMapping("main")
    public void putValue(@RequestParam Double currencyAmount, Model model) {
        Double totalAmount = currencyAmount * rateCoefficient;
        String s_totalAmountBounded = String.format("%.2f", totalAmount);
        Double totalAmountBounded = Double.parseDouble(s_totalAmountBounded.replace(',', '.'));
        model.addAttribute("totalAmount", totalAmountBounded);
        history.setSourceSum(currencyAmount);
        history.setConvertedSum(totalAmountBounded);
        historyRepo.save(history);
    }
}