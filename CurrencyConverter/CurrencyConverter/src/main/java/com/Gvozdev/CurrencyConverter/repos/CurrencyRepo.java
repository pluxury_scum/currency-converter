package com.Gvozdev.CurrencyConverter.repos;

import com.Gvozdev.CurrencyConverter.domain.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrencyRepo extends CrudRepository<Currency, Long> {
    List<Currency> findByCharCode(String charcode);
}
